package visao;

import java.awt.GridLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import modelo.Tabuleiro;

@SuppressWarnings("serial")
public class PainelTabuleiro extends JPanel {
    public PainelTabuleiro(Tabuleiro tab) {
        setLayout(new GridLayout(tab.getLinhas(), tab.getColunas()));

        tab.paraCada(c -> add(new BotaoCampo(c)));

        tab.registrarObservador(e -> {
            SwingUtilities.invokeLater(() -> {
                if(e.isGanhou()) {
                    JOptionPane.showMessageDialog(this, "Ganhou!");
                }else {
                    JOptionPane.showMessageDialog(this, "Perdeu!");
                }

                tab.reiniciar();
            });
        });
    }
}